#ifndef AXTERMTCP_H
#define AXTERMTCP_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class AXTermTcp; }
QT_END_NAMESPACE

class AXTermTcp : public QMainWindow
{
    Q_OBJECT

public:
    AXTermTcp(QWidget *parent = nullptr);
    ~AXTermTcp();

private:
    Ui::AXTermTcp *ui;
};
#endif // AXTERMTCP_H
